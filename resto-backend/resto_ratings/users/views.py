from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny
from .models import User
from .permissions import IsUserOrReadOnly
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework import generics
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response



from .serializers import CreateUserSerializer, UserSerializer


class UserViewSet(generics.RetrieveUpdateAPIView,
                  viewsets.GenericViewSet):
    """
    Updates and retrieves user accounts
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        """
        Only allow creation of restaurant for admin users
        """
        if self.action == 'list':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class UserCreateViewSet(mixins.CreateModelMixin,
                        generics.ListAPIView,
                        viewsets.GenericViewSet):
    """
    Creates user accounts
    """
    queryset = User.objects.all()
    serializer_class = CreateUserSerializer
    # permission_classes = (AllowAny,)

    def get_permissions(self):
        """
        Only allow creation of restaurant for admin users
        """

        if self.action == 'list':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = (AllowAny,)

        return [permission() for permission in permission_classes]


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'auth_token': token.key,
            'username': user.username,
            'is_staff': user.is_staff
        })
