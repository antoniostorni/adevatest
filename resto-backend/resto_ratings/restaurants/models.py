from django.db import models
import uuid
from resto_ratings.users.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import (Avg,
                              Max,
                              Min,
                              )


# Create your models here.

class Restaurant(models.Model):
    """
    Restaurant model
    """

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)

    name = models.CharField('Name', max_length=120)
    average_rating = models.FloatField(default=1)

    def __str__(self):
        return self.name

    def max_rating(self):
        return self.ratings.aggregate(Max('rating'))['rating__max']

    def min_rating(self):
        return self.ratings.aggregate(Min('rating'))['rating__min']

    def latest_review(self):
        try:
            return self.ratings.latest('date')
        except:
            return None

    def update_ratings(self):
        self.average_rating = self.ratings.aggregate(Avg('rating'))['rating__avg']
        if not self.average_rating:
            self.average_rating = 1
        self.save()


class Rating(models.Model):
    """
    Restaurant model
    """

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    date = models.DateTimeField(auto_now_add=True, editable=False)
    comment = models.TextField(max_length=240)
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurant, related_name='ratings', null=False, blank=False,on_delete=models.CASCADE)
    rating = models.PositiveIntegerField(default=5, validators=[MinValueValidator(1), MaxValueValidator(5)])

    def __str__(self):
        return f'{self.user} - {self.restaurant}'

    def save(self, *args, **kwargs):
        # Re-calculates the restorant avg rating on rating save
        super(Rating, self).save(*args, **kwargs)
        self.restaurant.update_ratings()

    def delete(self, *args, **kwargs):
        # Re-calculates the restorant avg rating on rating delete
        super(Rating, self).delete(*args, **kwargs)
        self.restaurant.update_ratings()

    class Meta:
        # Allow only one rate per user per restaurant
        unique_together = [['user', 'restaurant']]





