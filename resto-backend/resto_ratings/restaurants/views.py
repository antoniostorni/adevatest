from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework import generics
from .models import (Restaurant,
                     Rating,
                     )
from .serializers import (RestaurantSerializer,
                          RatingSerializer,
                          RatingCreateSerializer,
                          )


class RestarurantsViewSet(generics.ListAPIView,
                          generics.CreateAPIView,
                          generics.DestroyAPIView,
                          viewsets.GenericViewSet):
    """
    Retrieves restaurants
    """
    # queryset = Restaurant.objects.all()
    queryset = Restaurant.objects.order_by('-average_rating')
    serializer_class = RestaurantSerializer

    def get_permissions(self):
        """
        Only allow creation of restaurant for admin users
        """
        if self.action == 'list':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class RatingViewSet(generics.CreateAPIView,
                    generics.DestroyAPIView,
                    generics.ListAPIView,
                    viewsets.GenericViewSet):
    """
    Creates and deletes Rating
    """
    queryset = Rating.objects.all()

    def get_serializer_class(self):
        if self.action == "create":
            return RatingCreateSerializer
        else:
            return RatingSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_permissions(self):
        """
        Only allow creation of restaurant for admin users
        """
        if self.action == 'create':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
