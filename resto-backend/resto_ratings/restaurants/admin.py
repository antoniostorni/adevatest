from django.contrib import admin
from .models import *


class ClientAdmin(admin.ModelAdmin):
    pass


class RestaurantAdmin(admin.ModelAdmin):
    pass


class RatingAdmin(admin.ModelAdmin):
    pass


admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Rating, RatingAdmin)
