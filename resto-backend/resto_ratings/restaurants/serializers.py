from rest_framework import serializers
from .models import (Restaurant,
                     Rating,
                     )


class RatingSerializer(serializers.ModelSerializer):
    user = serializers.CharField(read_only=True, source='user.username')
    restaurant = serializers.CharField(read_only=True, source='restaurant.name')

    class Meta:
        model = Rating
        fields = ('id', 'date', 'comment', 'user', 'rating', 'restaurant')
        read_only_fields = ('user', 'date', 'id')


class RatingCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rating
        fields = ('comment', 'rating', 'restaurant', 'date')


class RestaurantSerializer(serializers.ModelSerializer):
    latest_review = RatingSerializer(required=False)

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'average_rating', 'max_rating', 'min_rating', 'latest_review')
        read_only_fields = ('average_rating', 'max_rating', 'min_rating', 'latest_review')
