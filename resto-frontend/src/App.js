import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Restaurants from "./components/restaurants";
import Ratings from "./components/ratings";
import RatingForm from "./components/ratingForm";
import Users from "./components/users";
import NotFound from "./components/notFound";
import NavBar from "./components/navBar";
import LoginForm from "./components/loginForm";
import RegisterForm from "./components/registerForm";
import Logout from "./components/logout";
import auth from "./services/authService";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import RestaurantForm from "./components/restaurantForm";

class App extends Component {
  state = { is_admin: null };

  componentDidMount() {
    const user = auth.getCurrentUser();
    const is_admin = auth.getIsAdmin();

    this.setState({ user, is_admin });
  }

  render() {
    const { user } = this.state;
    const { is_admin } = this.state;

    return (
      <React.Fragment>
        <ToastContainer />
        <NavBar user={user} is_admin={is_admin} />
        <main className="container">
          <Switch>
            <Route path="/register" component={RegisterForm} />
            <Route path="/login" component={LoginForm} />
            <Route path="/logout" component={Logout} />
            <Route path="/rate/:id" component={RatingForm} />
            <Route path="/new_restaurant/" component={RestaurantForm} />

            <Route
              path="/restaurants"
              render={props => <Restaurants {...props} is_admin={is_admin} />}
            />
            <Route path="/users" component={Users} />
            <Route path="/ratings" component={Ratings} />
            <Route path="/not-found" component={NotFound} />
            <Redirect from="/" exact to="/restaurants" />
            <Redirect to="/not-found" />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
