import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { saveRestaurant } from "../services/restaurantService";

class RestaurantForm extends Form {
  state = {
    data: {
      name: ""
    },
    errors: {}
  };

  schema = {
    name: Joi.string()
      .required()
      .label("Name")
  };

  doSubmit = async () => {
    await saveRestaurant(this.state.data);

    this.props.history.push("/restaurants");
  };

  render() {
    return (
      <div>
        <h1>Create Restaurant</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("name", "Name")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default RestaurantForm;
