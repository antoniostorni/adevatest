import React, { Component } from "react";
import Collapse from "react-bootstrap/Collapse";
import Rating from "./rating";
import { Link } from "react-router-dom";

class Restaurant extends Component {
  state = {};

  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }
  render() {
    const { open } = this.state;

    return (
      <div className="card">
        <div class="card-body">
          <h1 />
          <p>
            <button
              className="btn btn-primary btn-block"
              onClick={() => this.setState({ open: !open })}
              aria-controls="example-collapse-text"
              aria-expanded={open}
            >
              {this.props.children[3].name}
            </button>
            <p />
            <Collapse in={this.state.open}>
              <div id="example-collapse-text">
                <p className="card-text">
                  Average Rating:
                  <span className="badge badge-secondary">
                    {this.props.children[3].average_rating}
                  </span>
                </p>
                <p>
                  Highest Rating Review:
                  <span className="badge badge-secondary">
                    {this.props.children[3].max_rating}
                  </span>
                </p>
                <p>
                  Lowest Rating Review:
                  <span className="badge badge-secondary">
                    {this.props.children[3].min_rating}
                  </span>
                </p>
                <p>
                  <h3>Latest Review: </h3>
                  {this.props.children[3].latest_review && (
                    <Rating>
                      key={this.props.children[3].latest_review.id}
                      rating={this.props.children[3].latest_review}
                    </Rating>
                  )}
                </p>
              </div>
            </Collapse>{" "}
          </p>
          <Link to={`/rate/${this.props.children[3].id}`}>
            <button
              className="btn btn-secondary"
              aria-controls="example-collapse-text"
              aria-expanded={open}
            >
              Review Restaurant
            </button>
          </Link>
          {this.props.children[7] && (
            <button
              className="btn btn-danger pull-right"
              aria-controls="example-collapse-text"
              onClick={() => this.props.children[5](this.props.children[3])}
            >
              Delete Restaurant
            </button>
          )}
        </div>
      </div>
    );
  }
}

export default Restaurant;
