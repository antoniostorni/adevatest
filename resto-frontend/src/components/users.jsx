import React, { Component } from "react";
import { getUsers, deleteUser } from "../services/userService";
import User from "./user";

import _ from "lodash";

class Users extends Component {
  state = {
    users: []
  };

  async componentDidMount() {
    const { data: users } = await getUsers();
    console.log("users", users);
    this.setState({ users });
  }

  handleDelete = async user => {
    console.log("handle delete");
    console.log(user);
    const originalUsers = this.state.users;
    const users = originalUsers.filter(u => u.id !== user.id);
    this.setState({ users });

    try {
      await deleteUser(user.id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.setState({ users: originalUsers });
    }
  };

  render() {
    const { length: count } = this.state.users;

    if (count === 0) return <p>There are no movies in the database.</p>;

    const { data: users } = this.props;

    return (
      <div className="flex-large">
        {this.state.users.map(user => (
          <User>
            key={user.email}
            user={user}
            onDelete={this.handleDelete}
          </User>
        ))}
      </div>
    );
  }
}

export default Users;
