import React, { Component } from "react";
import { getRatings, deleteRating } from "../services/ratingService";
import Rating from "./rating";

class Ratings extends Component {
  state = {
    ratings: []
  };

  async componentDidMount() {
    const { data: ratings } = await getRatings();
    this.setState({ ratings });
  }

  handleDelete = async rating => {
    const originalRatings = this.state.ratings;
    const ratings = originalRatings.filter(r => r.id !== rating.id);
    this.setState({ ratings });

    try {
      await deleteRating(rating.id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.setState({ movies: originalRatings });
    }
  };

  render() {
    const { length: count } = this.state.ratings;

    if (count === 0) return <p>There are no ratings in the database.</p>;

    const { data: users } = this.props;

    return (
      <div className="flex-large">
        {this.state.ratings.map(rating => (
          <Rating>
            key={rating.id}
            rating={rating}
            onDelete={this.handleDelete}
          </Rating>
        ))}
      </div>
    );
  }
}

export default Ratings;
