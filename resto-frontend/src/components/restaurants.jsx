import React, { Component } from "react";
import {
  getRestaurants,
  deleteRestaurant
} from "../services/restaurantService";
import _ from "lodash";
import Restaurant from "./restaurant";
import { Link } from "react-router-dom";

class Restaurants extends Component {
  state = {
    movies: [],
    genres: [],
    restaurants: [],
    sortColumn: { path: "title", order: "asc" }
  };

  async componentDidMount() {
    const { data: restaurants } = await getRestaurants();
    this.setState({ restaurants });
  }

  handleDelete = async restaurant => {
    console.log("handle delete");
    console.log(restaurant);
    const originalRestaurants = this.state.restaurants;
    const restaurants = originalRestaurants.filter(r => r.id !== restaurant.id);
    this.setState({ restaurants });

    try {
      await deleteRestaurant(restaurant.id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.setState({ movies: originalRestaurants });
    }
  };

  handleLike = movie => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index] = { ...movies[index] };
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleGenreSelect = genre => {
    this.setState({ selectedGenre: genre, searchQuery: "", currentPage: 1 });
  };

  handleSearch = query => {
    this.setState({ searchQuery: query, selectedGenre: null, currentPage: 1 });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  render() {
    const { length: count } = this.state.restaurants;
    const { is_admin } = this.props;

    if (count === 0) return <p>There are no restaurants in the database.</p>;

    const { data: restaurants } = this.props;

    return (
      <div>
        {is_admin && (
          <div className="flex-large">
            <Link to="/new_restaurant/">
              <button type="button" class="btn btn-info">
                + Create New Restaurant
              </button>
            </Link>
          </div>
        )}

        <div className="flex-large">
          {this.state.restaurants.map(restaurant => (
            <Restaurant>
              key={restaurant.id}
              restaurant={restaurant}
              onDelete={this.handleDelete}
              is_admin={is_admin}
            </Restaurant>
          ))}
        </div>
      </div>
    );
  }
}

export default Restaurants;
