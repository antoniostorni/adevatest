import React, { Component } from "react";

class Rating extends Component {
  state = {};

  constructor(props) {
    super(props);
  }
  render() {
    console.log("aaaa", this.props);

    return (
      <div className="card">
        <div class="card-body">
          <h5 class="card-title">{this.props.children[3].restaurant}</h5>
          <h6 class="card-subtitle mb-2 text-muted">
            User: {this.props.children[3].user}
          </h6>
          <p class="card-text">
            <p>
              Rating:
              <span className="badge badge-secondary">
                {this.props.children[3].rating}
              </span>
            </p>
            <p>Comment: "{this.props.children[3].comment}"</p>
          </p>
          {this.props.children[5] && (
            <button
              className="btn btn-danger pull-right"
              aria-controls="example-collapse-text"
              onClick={() => this.props.children[5](this.props.children[3])}
            >
              Delete Rating
            </button>
          )}
        </div>
      </div>
    );
  }
}

export default Rating;
