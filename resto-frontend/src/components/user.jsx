import React, { Component } from "react";

class User extends Component {
  state = {};

  constructor(props) {
    super(props);
  }
  render() {
    console.log(this.props);
    return (
      <div className="card">
        <div class="card-body">
          <h5 class="card-title">{this.props.children[3].username}</h5>
          <p class="card-text" />

          <button
            className="btn btn-danger pull-right"
            aria-controls="example-collapse-text"
            onClick={() => this.props.children[5](this.props.children[3])}
          >
            Delete User
          </button>
        </div>
      </div>
    );
  }
}

export default User;
