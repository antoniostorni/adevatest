import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { saveRating } from "../services/ratingService";

class RatingForm extends Form {
  state = {
    data: {
      comment: "",
      rating: "",
      restaurant: this.props.match.params.id
    },
    errors: {}
  };

  schema = {
    restaurant: Joi.string(),
    comment: Joi.string()
      .required()
      .label("Comment"),
    rating: Joi.number()
      .required()
      .min(1)
      .max(5)
      .label("Rating 1-5")
  };

  async componentDidMount() {
    const restoid = this.props.match.params.id;
  }

  doSubmit = async () => {
    await saveRating(this.state.data);

    this.props.history.push("/restaurants");
  };

  render() {
    return (
      <div>
        <h1>Rate Restaurant</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("rating", "Rating (1 to 5)")}
          {this.renderInput("comment", "comment")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default RatingForm;
