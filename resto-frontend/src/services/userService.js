import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/users/";

function userUrl(id) {
  return `${apiEndpoint}${id}/`;
}

export function getUsers() {
  return http.get(apiEndpoint);
}

export function register(user) {
  return http.post(apiEndpoint, {
    email: user.username,
    password: user.password,
    username: user.name
  });
}

export function deleteUser(userId) {
  return http.delete(userUrl(userId));
}
