import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/restaurants/";

function restaurantUrl(id) {
  return `${apiEndpoint}${id}/`;
}

export function getRestaurants() {
  return http.get(apiEndpoint);
}

export function deleteRestaurant(restaurantId) {
  return http.delete(restaurantUrl(restaurantId));
}

export function saveRestaurant(restaurant) {
  return http.post(apiEndpoint, restaurant);
}
