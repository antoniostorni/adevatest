import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/ratings/";

function ratingUrl(id) {
  return `${apiEndpoint}${id}/`;
}

export function getRatings() {
  return http.get(apiEndpoint);
}

export function saveRating(rating) {
  return http.post(apiEndpoint, rating);
}

export function deleteRating(ratingId) {
  return http.delete(ratingUrl(ratingId));
}
