import http from "./httpService";

const apiEndpoint = "http://localhost:8000/api-token-auth/";
const tokenKey = "token";
const userKey = "user";
const userIsAdminKey = "admin";

http.setAuthToken(getToken());

export async function login(username, password) {
  const { data: user } = await http.post(apiEndpoint, { username, password });
  localStorage.setItem(tokenKey, user["auth_token"]);
  localStorage.setItem(userKey, user["username"]);
  localStorage.setItem(userIsAdminKey, user["is_staff"]);
}

export function loginSave(user) {
  console.log("esto llega", user);
  localStorage.setItem(tokenKey, user["auth_token"]);
  localStorage.setItem(userKey, user["username"]);
  localStorage.setItem(userIsAdminKey, user["is_staff"]);
}

export function logout() {
  localStorage.removeItem(tokenKey);
  localStorage.removeItem(userKey);
  localStorage.removeItem(userIsAdminKey);
}

export function getCurrentUser() {
  try {
    var user = {
      name: localStorage.getItem(userKey)
    };
    if (user.name == null) {
      return null;
    }
    return user;
  } catch (ex) {
    return null;
  }
}

export function getIsAdmin() {
  try {
    const isAdmin = localStorage.getItem(userIsAdminKey);
    if (isAdmin == "true") {
      return true;
    }
  } catch (ex) {
    return null;
  }
}

export function getToken() {
  try {
    const token = localStorage.getItem(tokenKey);
    return token;
  } catch (ex) {
    return null;
  }
}

export default {
  login,
  loginSave,
  logout,
  getCurrentUser,
  getIsAdmin
};
