Test Project
=======================

Test project to review restaurants.


Backend
--------

Based on Django Rest Framework

  $ pip install -r requirements.txt

  $ python manage.py runserver

Frontend
--------------

   $ npm install

   $ npm start


